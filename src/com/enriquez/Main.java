package com.enriquez;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
        System.out.println("** This is for Arrays **");
        // Arrays

        // Syntax
        // dataType[] identifier = new dataType[arrayLength];

        int[] numberList = new int[3];

        // Add new items in array
        numberList[0] = 10;
        numberList[1] = 25;
        numberList[2] = 40;
        // numberList[3] = 55;

        System.out.println(numberList[1]); // Will display array content of index

        System.out.println(numberList);  // Will display the memory value of the array
        System.out.println(Arrays.toString(numberList)); // Will Display all array content in a string format

        // Alternative way of creating a traditional array

        //Syntax
        // dataType[] identifier = { elements, elements, ...}

        String[] surnameList = { "Romeo", "Cabagnot", "Perez" };

        System.out.println(surnameList[0]);

        System.out.println(Arrays.toString(surnameList));

        //Multidimensional array

        //Syntax
        // dataType[][] identifier = new dataType[arrayLength1][arrayLength2];

        String[][] classRoom = new String[3][3];

        // First Row
        classRoom[0][0] = "Athos";
        classRoom[0][1] = "Porthos";
        classRoom[0][2] = "Artemis";

        // Second Row
        classRoom[1][0] = "Mickey";
        classRoom[1][1] = "Donald";
        classRoom[1][2] = "Goofy";

        // Third Row
        classRoom[2][0] = "Harry";
        classRoom[2][1] = "Ron";
        classRoom[2][2] = "Hermione";

        System.out.println(classRoom);
        System.out.println(Arrays.deepToString(classRoom)); // Display multidimensional arrays value

        // ArrayList ---> dynamic compared to traditional Array

        // Syntax
        // Arraylist<dataType> identifier = new ArrayList<dataType>();

        ArrayList<String> studentList = new ArrayList<String>();

        // Adding elements
        studentList.add("John");
        studentList.add("Paul");
        studentList.add("Matthew");
        // studentList.add("Jude"); // Will dynamically adjust the storage allocation

        System.out.println(studentList);

        ArrayList<Integer> ageList = new ArrayList<Integer>();

        ageList.add(23);

        System.out.println(ageList);

        // Retrieve elements
        System.out.println(studentList.get(0));
        System.out.println(studentList.get(2));

        // Updating elements
        System.out.println(studentList.get(1));
        //Syntax
        //studentList.set(index, newValue);
        studentList.set(1, "Peter");
        System.out.println(studentList.get(1));

        //Deleting elements
        studentList.remove(2);
        System.out.println(studentList);

        // Mini-activity
        // 1) Clear all the values stored in student list
        // studentList.clear();
        // 2) Display to the console the updated studentList
        // System.out.println(studentList);
        // 3) Display the current length of the studentList to the console
        // System.out.println(studentList.size());

        studentList.clear();
        System.out.println(studentList);
        System.out.println(studentList.size());

        // ArrayList with default values
        ArrayList<String> employeesList = new ArrayList<>(Arrays.asList("Giannis", "Chris", "Jrue"));
        System.out.println(employeesList);

        System.out.println("** This is for HashMaps **");

        // HashMaps --> is a collection of items with key-value pairs

        // Syntax
        // HashMap<keyDataType, valueDataType> identifier = new HashMap<keyDataType, valueDataType>();

        HashMap<String, String> jobPositionsList = new HashMap<String, String>();

        // Creating elements
        jobPositionsList.put("Giannis", "Power Forward");
        jobPositionsList.put("Chris", "Shooting Guard");
        jobPositionsList.put("Jrue", "Point Guard");

        System.out.println(jobPositionsList);

        // Retrieving elements
        System.out.println(jobPositionsList.get("Giannis"));

        // Updating elements
        jobPositionsList.replace("Jrue","Score-First Guard");
        System.out.println(jobPositionsList);

        // Removing elements
        jobPositionsList.remove("Chris");
        System.out.println(jobPositionsList);
        System.out.println(jobPositionsList.size());

        // Mini-Activity Retrieve all HashMap keys
        // Display to the console all of the existing keys of jobPositionsList
        System.out.println(jobPositionsList.keySet());

        // Mini-Activity
        // Create a HashMap of grades list.
        HashMap<String, Integer> gradeList = new HashMap<String, Integer>();

        // The key is the lastname of a student and grade is the value.
        // Create 3 mockup items for the list.
        gradeList.put("Dela Cruz", 88);
        gradeList.put("Belga", 90);
        gradeList.put("Acuzar", 77);

        // Display the mockup to the console.
        System.out.println(gradeList);
    }
}

